package com.wlj.demo.service;

import com.wlj.demo.entity.User;
import com.wlj.demo.mapper.UserMapper;
import org.springframework.stereotype.Service;
 
import javax.annotation.Resource;
import java.util.List;
 
/**
 * 用户service实现类
 */
@Service
public class UserServiceImpl implements UserService {
 
	@Resource
	private UserMapper userMapper;
 
	public User findByUsernameAndPwd(String name, String pwd) {
 
		return userMapper.findByUsernameAndPwd(name, pwd);
	}
 
	public List<User> find(User user){
 
		return userMapper.find(user);
	}
 
 
	public void add(User user) {

		userMapper.add(user);
	}
	
	public void update(User user) {

		userMapper.update(user);
	}
 
	public void delete(String id) {

		userMapper.delete(id);
	}
}